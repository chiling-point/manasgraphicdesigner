var myIndex = 0;
carousel();

function carousel() {
  var i;
  var x = document.getElementsByClassName("mySlides");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  myIndex++;
  if (myIndex > x.length) {myIndex = 1}    
  x[myIndex-1].style.display = "block";  
  setTimeout(carousel, 2500);    
}

var slides = document.querySelectorAll(".slide");
var dots = document.querySelectorAll(".dot");
var index = 0;


function prevSlide(n){
  index+=n;
  console.log("prevSlide is called");
  changeSlide();
}

function nextSlide(n){
  index+=n;
  changeSlide();
}

changeSlide();

function changeSlide(){
    
  if(index>slides.length-1)
    index=0;
  
  if(index<0)
    index=slides.length-1;
  
  
  
    for(let i=0;i<slides.length;i++){
      slides[i].style.display = "none";
      
      dots[i].classList.remove("active");
      
      
    }
    
    slides[index].style.display = "block";
    dots[index].classList.add("active");

  

}

(function() {
  "use strict";
  //user input text box
  let userInput = document.querySelector("#userInput");

  userInput.addEventListener("keyup", filterContacts);

  function filterContacts(event) {
    //Get the user input
    let userInputText = userInput.value;
    //convert it to lowercase
    userInputText = userInputText.toLowerCase();

    //get the ul and all the li children inside it
    let contactList = document.querySelector("#contactList");
    let contactListChildren = contactList.querySelectorAll(
      "li.collection-item"
    );

    //loop through all the list items and search for the name
    contactListChildren.forEach(function(currentValue, index, array) {
      //get the contact name text of each contact we are cycling through
      let currentContact = contactListChildren[index].innerText;
      //convert contact name text lowercase
      currentContact = currentContact.toLowerCase();

      //if the contact is found
      if (currentContact.search(userInputText) === 0) {
        console.log("found in " + currentContact);
        contactListChildren[index].style.display = "block";
      } else {
        contactListChildren[index].style.display = "none";
      }
    });
  }
})();

